import type { GetStaticProps, NextPage } from "next";
import Head from "next/head";
import Banner, { BannerContentType } from "../components/Banner";
import CategoryPicker from "../components/CategoryPicker";
import FeaturedBlogs, { Blogsype } from "../components/FeaturedBlogs";
import FeaturedProducts from "../components/FeaturedProducts";
import { ProductItemType } from "../components/ProductItem";

interface Props {
  bannerData: BannerContentType
  featuredProducts: ProductItemType[]
  featuredBlogs:Blogsype[]
}

const Home: NextPage<Props> = ({ bannerData, featuredProducts ,featuredBlogs}) => {
  return (
    <>
      <Head>
        <title>Store</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Banner preTitle={bannerData.preTitle} title={bannerData.title} />

      <CategoryPicker />

      <FeaturedProducts data={featuredProducts} />

      <FeaturedBlogs data={featuredBlogs}/>
    </>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const bannerDataRes = await fetch('https://boutique-server-api.herokuapp.com/banner_content')
  const bannerData = await bannerDataRes.json()

  const featuredProductsRes = await fetch('https://boutique-server-api.herokuapp.com/products/?_limit=4')
  const featuredProducts = await featuredProductsRes.json()

  const featuredBlogsRes = await fetch('https://boutique-server-api.herokuapp.com/blogs/?_limit=3')
  const featuredBlogs = await featuredBlogsRes.json()


  return {
    props: {
      bannerData,
      featuredProducts,
      featuredBlogs,
    }
  }
}

export default Home;
